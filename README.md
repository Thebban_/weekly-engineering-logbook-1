# Weekly Engineering Logbooks

<div align="center">

<img src="https://www.upm.edu.my/imej/content/328_bm.jpg" width=400 align=middle>

</div>




<div align='center'>

**DEPARTMENT OF AEROSPACE ENGINEERING**

**EAS4947-1:INTEGRATED DESIGN PROJECT**

</div>

<div align="center">

**Weekly Logs**

</div>

<br />
<br />
<br />



**NAME          : THEBBAN A/L SEENIVASAN**

**MATRIC NUMBER : 196382**

**SUBSYSTEM : PAYLOAD DESIGN & SPRAYER**

## TABLE OF CONTENT
- [WEEK 2](#week-2)
- [WEEK 3](#week-3)
- [WEEK 4](#week-4)
- [WEEK 5](#week-5)
- [WEEK 6](#week-6)
- [WEEK 7](#week-7)

## Week 2
| Header | Description |
| ------ | ----------- |
| Agenda | The week’s agenda was to familiriaze and corall members to form groups which were assigned to us by Dr.Salah based on our interests in subsystems as well as individual personalities that would balance the groups as a whole. There are two groups in which one would host members who were assigned to the same subsystem and another would hold members from different subsystems to provide updates to each other about their own subsystem as well as help monitor weekly logs. I was assigned to payload as my subsystem group and group 5 as my ‘cohesion’ group. |
| Goals | The goals with this week were to form a gantt chart that would highlight the research,design and build processes for the components of our subsystems. |
| Important Decisions Made | We decided to have a short meeting using discord to discuss and edit the gantt chart to better suit our capabilites as well as fulfill requirements with regards to the timeline where our subsystem group would complete all tasks regarding the payload by week 14 in time for the final test. |
| Justification | This was done to ensure all members of our subsystem were familiar with the timeline of the payload project and provide insight which would be helpful towards the process or otherwise would be missed if one person were to create said gantt chart. |
| Impact on Project | We were able to complete a gantt chart and intergrate it with all the other subsystems. The gantt chart link is provided below: https://docs.google.com/spreadsheets/d/1j9F4gbNZMCxQDzfhFpA2i7lVFxfhL9kVc-zxi1asfZs/edit#gid=1115838130  |
| Next Step | The next step is to perform research on agricultural drones and their funtionalities in crop spraying to provide ourselves with a background on the closest competitor to the HAU. |

## Week 3 
| Header | Description |
| ------ | ----------- |
| Agenda | <ul><li>This week’s agenda was to learn and understand Gitlab and submit tasks relating to gitlab.</li><li>As for the subsystem, a site visit to aerodyne was planned to gain insight on commercial agricultural drones and their sprayer systems |
| Goals | <ul><li>The goals were to submit an introductory readme file filled information about ourselves using available presets for font changes, tables, link embedding for pictures as well as lists.</li><li>For the subsystem, the goals were to collect information on agricultural drones such as the DJI Agras T20 and T16 |
| Important Decisions Made | <ul><li>I decided to learn about gitlab functions for pulling and merging files related to our subsystems as well as weekly logs during a briefing held on Tuesday.  I also watched guide videos about gitlab functions to understand what I missed during the briefing.</li><li>We decided to understand the sprayer systems for such drones as well as their operations in terms of tank placement, sprayer spacing, operation altitude and field mapping. |
| Justification | <ul><li>This was done as our weekly logs would be coded and merged with gitlab as  well as updates on our subsystem as well as other subsystems would be accesible  through gitlab.</li><li>We decided to understand these aspects from the Agras T20 and T16 to adapt said systems to the HAU to produce similar or better outputs with regards to the payload while keeping parameters at a reasonable limit to achieve targets in spraying paddy crops. |
| Impact on Project | <ul><li>The impact made on using gitlab as the platform for the Intergrated Design Project 2021 was to provide us with a centralized platform to post and provide updates about our subsystem’s progress for general viewing which would bring everyone on the same page with regards to the construction of the HAU. Using gitlab also provides experience which would be valuable in a workplace environment where group projects use gitlab for remote work updates.</li><li>We were able to gain relevant information from the sprayer system of said commercial agricultural drones which would be modified to use in the HAU |
| Next Step | The next step is to find and compare components that would be used for the payload system in the HAU as well as to start calculations on the sprayer parameters to influence our choices in buying components. |

## Week 4 
| Header | Description |
| ------ | ----------- |
| Agenda | This week’s agenda was to find and collect components off online marketplaces to compare and contrast in terms of capability requirements and pricing. |
| Goals | The goals were to select the best available components which fall within our requirements for each components and discuss whether they would be intergrated into the design for the payload. |
| Important Decisions Made | I, along with Yamunan decided to form an excel sheet from sprayer calibration calculations provided by Dr.Salah while my group members formed a component tracker to add in potential component candidates for the pump, sprayer nozzles and pesticide tank. We also performed research on the parameter estimations in terms of the Gallons Per Acre (GPA) requirements for several types of pesticides, Nozzle spacing based on sprayer swath requirements for even pesticide distribution and Field speed estimation with a little help from our friends in the propulsion subsystem |
| Justification | The calculations were done to provide a benchmark parameter for the Gallons Per Minute (GPM) requirement for pump selection based on estimated calculations of the parameters mentioned above. As for the nozzle, the set distribution angle (swath angle) was needed to set estimations on the nozzle spacing and boom height. Several options for both components were compiled in the tracker and all available options for the tank (which were used on agricultural drones) were provided in the tracker for a discussion with Mr.Fiqri with regards to the permitted weight allocation for the payload system. The tracker link is provided below: https://docs.google.com/spreadsheets/d/1f6qWolqoLCnQdBIxJAotW_M_l-wg4oBWc64O2xBw39E/edit?usp=sharing |
| Impact on Project | All available options from the tracker as well as our choices for the nozzle and pump were provided to Mr.Fiqri. The feedback provided from Mr.Fiqri was to employ a smaller pump due to the slower speed of the HAU and to use a smaller tank (approximately 5 Litres) to account for weight reduction and system feasibility to make sure our proposed system works before scaling up. |
| Next Step | The next step is to make changes to the calculations to account for the suggestions provided by Mr.Fiqri and to perform additional research on the sprayer system to better understand the effects of slower speed on crop spraying. |

## Week 5 
| Header | Description |
| ------ | ----------- |
| Agenda | This weeks agenda was to make changes to calculations based on suggestions provided from Mr.Fiqri as well as to start the design of the payload system including the tank holder and boom extensions. |
| Goals | The goals this week were to adjust parameters in the calculations to include a smaller pump and a smaller tank as well as design a twin boom extension for the nozzle placement on all four corners of the HAU.|
| Important Decisions Made | Decisions were made to provide several boom design variants to present to Dr.Salah during our weekly discussions and additional research was done with regards to pesticide mixing to account for the smaller tank. |
| Justification | The boom placement design variants were made for discussion amongst groupmates for a preferred variant to present during the weekly discussions. The newly calculated parameters warranted additonal searched on online marketplaces to find a pump that would distribute the pesticide mixture at a slower rate (1.32 GPM) |
| Impact on Project | Our findings and design variants were presented to Dr.Salah during the weekly discussions and remarks were given by Dr.Salah in terms of providing the calculations in a statistical sense to show the advantages and downsides for component changes as well as concern over the four corner nozzle placement design as the downwash from the propellors atop the HAU would tamper with the pesticide distribution from the nozzle. Hence, a different variant where a single boom in the middle of the HAU with 4 corresponding nozzles was selected to counteract the downwash effect as well as improve weight distribution. |
| Next Step | The next step is to finalize components and calculations regarding said components to send in orders for selected components. |

## Week 6 
| Header | Description |
| ------ | ----------- |
| Agenda | The agenda for this week is to finalize our components as well as subsequently come up with the final calculations on the sprayer system. |
| Goals | The goals for this week were to finish up the pesticide mixing ratio calculation for a pesticide variant specific for paddy (lambda cylathorin) , come up with a final design of what the assembled payload system might look like and to conduct inquiries to sellers for availability of components. |
| Important Decisions Made | We decided to split tasks amongs group members according to the matters needed to be dealt with. The list for group members and their task allocation is mentioned below:-<br /><ul><li>Estimated Weight and Field Speed Query (Thebban)</li><li>Calculations Transfer to Google Sheet (Yamunan)</li><li>Mounting + Structure Design (Aleesya + Hanis)</li><li>Nozzle Jet Swath Query with Seller (Irfan)</li><li>Pesticide Mixing Ratio Calculation and conversion to GPA (Rafi)</li><li>Total Calculation completion (Thebban + Yamunan)</li><li>Presentation prepration for week  6(Rafi) |
| Justification | This was done to ensure faster results in a more cooperative environment in our subsytem groups as well as to ease burden on those who were performing additional tasks beforehand |
| Impact on Project | The design of the payload system (Tank + Tank holder scaffolding + Booms) was finalized with many of the materials used to build the  system were decided as well. We decided to use electrical PVC pipes for the boom and tank scaffolding whic would be joined together using PVC connectors, flex tubes for the pump-to-nozzle piping with cable ties to hold said flex tubes in place. The calculations were completed as well. The payload calculation link is provided below:<br />https://docs.google.com/spreadsheets/d/1Ks4jATKlu6y6-r6O5OB6SzaOrM-6PxI-FWul_H_DW1I/edit?usp=sharing |
| Next Step | The next step was to update the component tracker with parts for the boom and tank scaffolding as well as the tank and to performs weight calculations for the subsystem components. |

## Week 7 
| Header | Description |
| ------ | ----------- |
| Agenda | The agenda for this week is to calculate the total weight of components present in the payload subsystem as well as to provide the component order list to Mr.Fiqri. |
| Goals | The goals for this week are to make sure that total weight of the components that are to be bought are under the 7 Kg weight limit given to us by Dr.Salah and to finalize all parameters present in the calculations to select final components for ordering. |
| Important Decisions Made | We decided to buy the components for the tank, tank scaffolding and the booms at a nearby hardware store. |
| Justification | This is to reduce waiting times for component arrival and using the wait time to complete the structural parts of our subsystem to warrant more time for testing and development. |
| Impact on Project | The total weight, final calculations and final component list were presented during our weekly discussions with Dr.Salah and the go ahead was given to send in orders for the components as well as the weight requirements were fulfilled as the total estimated weight of our components totaled to 6.89 kilograms. |
| Next Step | The next step is to complete the fabrication of all structural components during the mid semester break, so as to proceed with the assembly of components for our subsystem once they arrive on a projected timeline by week 8. |

